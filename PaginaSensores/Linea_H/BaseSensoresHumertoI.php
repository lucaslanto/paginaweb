<!DOCTYPE html>
<!--[if lt IE 7]><html lang="es" class="lt-ie10 lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if IE 7]><html lang="es" class="lt-ie10 lt-ie9 lt-ie8"><![endif]-->
<!--[if IE 8]><html lang="es" class="lt-ie10 lt-ie9"> <![endif]-->
<!--[if IE 9]><html lang="es" class="lt-ie10"> <![endif]-->
<!--[if gt IE 9]><!--> <html lang="es"> <!--<![endif]-->
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="BAstrap3/bastrap3/bootstrap.min.css" rel="stylesheet">
	<meta name="viewpoint" content "width=device-width, initial-scale = 1.0">
	<link rel = "stylesheet" type = "text/css" href = "css/boostrapp-responsive.css">
	<link rel="shortcut icon" href="bastrap3/favicon.ico">
    <link rel="apple-touch-icon-precomposed" href="bastrap3/favicon-mobile.png">
    <link rel="stylesheet" href="bastrap3/bootstrap.min.css">
    <link rel="stylesheet" href="bastrap3/bastrap.css">
	<title>Humberto I</title>

    <!-- ESTILOS EXTRA -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <header class="navbar navbar-primary navbar-top">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-sm-6">
            <a class="navbar-brand bac-header" href="index.html">Buenos Aires Ciudad</a>
          </div>
          <div class="col-md-6 col-sm-6">
            <h5 class="sub-brand">En todo estás vos</h5>
          </div>
        </div>
      </div>
    </header>
<div class="collapse navbar-collapse" id="main-nav">
            <ul class="nav navbar-nav navbar-right">
              <li><a href="D:\Pagina web\LineaA.php"><--Volver atras</a></li>
            </ul>
          </div>	
<!-- CONTENIDO -->
    <main class="main-container">
      <div class="container">
            <section>
              <h2 align = 'center'>Estacion Humberto I - Administracion de sensores</h2>
              <div class="table-responsive">
                <table class="table table-striped">
                  <thead>
                    <tr>
                      <th>Tipo</th>
                      <th>Nombre</th>
                      <th>Estado</th>
                  </tr>
                 
                  </thead>
                  <tbody>
                         <?php 
                     include 'Conexion.php';
                     ?>

                  </tbody>
                  <br> 
                </table>
              </div>


        <br>
        <br>
	<div>		  
			  <div class="panel-group" id="accordion">
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
          ¡Donde se ubica la estacion?
        </a>
      </h4>
    </div>
    <div id="collapseOne" class="panel-collapse collapse in">
      <div class="panel-body">
        La estacion se encuentra en la Av. Jujuy al 1100. 
      </div>
    </div>
  </div>
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
          ¿En que barrio esta ubicado?
        </a>
      </h4>
    </div>
    <div id="collapseTwo" class="panel-collapse collapse">
      <div class="panel-body">
        Se encuentra en barrio de San Cristobal, ubicado en la comuna 3.
      </div>
    </div>
  </div>
  <div>
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
          ¿Como llegar?
        </a>
      </h4>
    </div>
    <div id="collapseThree" class="panel-collapse collapse">
      <div class="panel-body">
        Los colectivos que pasan por la estacion son: 
		<div>Si desea, puede descargar "BA como llego" desde el siguiente 
		<a href = "https://play.google.com/store/apps/details?id=ar.gob.buenosaires.comollego&hl=es_419" >link</a></div>
		</div>
    </div>
	</div>
	</div>
</div>

      
	
<footer>
      <div class="footer">
        <div class="container">
          <div class="row">
            <div class="col-md-6 col-sm-6">
              <a class="navbar-brand bac-footer" href="http://www.buenosaires.gob.ar" target="_blank">Buenos Aires Ciudad</a>
            </div>
            <div class="col-md-5 col-sm-6">
              <div class="sub-brand">
                <p>Dirección General de Gobierno Electrónico<br />
                <span class="text-muted">Ministerio de Modernización E Innovacion Tecnologica</span></p>
				<p>Subtes de la ciudad de Buenos Aires<br />
			  </div>
            </div>
          </div>
        </div>
      </div>
	  
    </footer>
	<script src="bastrap3/jquery.min.js"></script>
    <script src="bastrap3/bootstrap.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <!-- JAVASCRIPT EXTRA -->
  </body>
</html>