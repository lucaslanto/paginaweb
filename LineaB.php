<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Linea B</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="BAstrap3/bastrap3/bootstrap.min.css" rel="stylesheet">
	<meta name="viewpoint" content "width=device-width, initial-scale = 1.0">
	<link rel = "stylesheet" type = "text/css" href = "css/boostrapp-responsive.css">
	<link rel="shortcut icon" href="bastrap3/favicon.ico">
    <link rel="apple-touch-icon-precomposed" href="bastrap3/favicon-mobile.png">
    <link rel="stylesheet" href="bastrap3/bootstrap.min.css">
    <link rel="stylesheet" href="bastrap3/bastrap.css">
    <link rel="stylesheet" href="button.css">
	
	
  </head>
  
  <body background = ''>
  <header class="navbar navbar-primary navbar-top">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-sm-6">
		
		  
            <a class="navbar-brand bac-header" align = 'left' href="index.html">Buenos Aires Ciudad</a>
          </div>
		  
          <div class="col-md-6 col-sm-6">
            <h5 class="sub-brand">En todo estás vos</h5>
          </div>
        </div>
      </div>
    </header>
	<div class="collapse navbar-collapse" id="main-nav">
            <ul class="nav navbar-nav navbar-right">
              <li><a href="index.html">Volver al inicio</a></li>
            </ul>
          </div>	
	<div align = "center"> <h2> Linea de Subte: <img src = "Imagenes/Linea_B_subte.png" ></h2></div>
  <div> <h2 align = "center"><font color = "black" >Seleccione la estacion que desea verificar</h2> </font> </div>
  <br>
  
  <center>
   <button type="button" class="btn btn-sm btn-danger"onclick="window.open('PaginaSensores/Linea_B/Rosas.php');">Rosas</button>
 <button type="button" class="btn btn-sm btn-danger" onclick="window.open('PaginaSensores/Linea_B/Echeverria.php');">Echeverria</button>
 <button type="button" class="btn btn-sm btn-danger"onclick="window.open('PaginaSensores/Linea_B/ParqueChas.php');">Chas</button>
 <button type="button" class="btn btn-sm btn-danger"onclick="window.open('PaginaSensores/Linea_B/Tronador.php');">Tronador</button>
 <button type="button" class="btn btn-sm btn-danger"onclick="window.open('PaginaSensores/Linea_B/Lacroze.php');">Lacroze</button>
 <button type="button" class="btn btn-sm btn-danger"onclick="window.open('PaginaSensores/Linea_B/Dorrego.php');">Dorrego</button>
 <button type="button" class="btn btn-sm btn-danger"onclick="window.open('PaginaSensores/Linea_B/Malabia.php');">Malabia</button>
 <button type="button" class="btn btn-sm btn-danger"onclick="window.open('PaginaSensores/Linea_B/Gallardo.php');">Angel Gallardo</button>
 <button type="button" class="btn btn-sm btn-danger"onclick="window.open('PaginaSensores/Linea_B/Medrano.php');">Medrano</button>
 <button type="button" class="btn btn-sm btn-danger"onclick="window.open('PaginaSensores/Linea_B/Gardel.php');">Carlos Gardel</button>
 <button type="button" class="btn btn-sm btn-danger"onclick="window.open('PaginaSensores/Linea_B/Pueyrredon.php');">Pueyerredon</button>
 <button type="button" class="btn btn-sm btn-danger"onclick="window.open('PaginaSensores/Linea_B/Pasteur.php');">Pasteur</button>
 <button type="button" class="btn btn-sm btn-danger"onclick="window.open('PaginaSensores/Linea_B/Callao.php');">Callao</button>
 <button type="button" class="btn btn-sm btn-danger"onclick="window.open('PaginaSensores/Linea_B/Uruguay.php');">Uruguay</button>
 <button type="button" class="btn btn-sm btn-danger"onclick="window.open('PaginaSensores/Linea_B/Pellegrini.php');">Pellegrini</button>
 <button type="button" class="btn btn-sm btn-danger"onclick="window.open('PaginaSensores/Linea_B/Florida.php');">Florida</button>
 <button type="button" class="btn btn-sm btn-danger"onclick="window.open('PaginaSensores/Linea_B/Alem.php');">Alem</button>

 </center>
 
<br>
<br>
<br>
<center>
<div style="width: 100%; overflow-x: auto; overflow-y: hidden;"> <img src = "Imagenes/EsquemaLineaB.jpg" ></div>
</center>
<!-- Split button -->

  <br>
  <br>
  
  <footer>
      <div class="footer">
        <div class="container">
          <div class="row">
            <div class="col-md-5 col-sm-6">
              <a class="navbar-brand bac-footer" href="http://www.buenosaires.gob.ar" target="_blank">Buenos Aires Ciudad</a>
            </div>
            <div class="col-md-6 col-sm-6">
              <div class="sub-brand">
                <p>Dirección General de Gobierno Electrónico<br />
                <span class="text-muted">Ministerio de Modernización E Innovacion Tecnologica</span></p>
				<p>Subtes de la ciudad de Buenos Aires<br />
			  </div>
            </div>
          </div>
        </div>
      </div>
	  
    </footer>
	
	

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>