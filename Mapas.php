<!DOCTYPE html>
<html lang="es">
  <head>
	<title>Mapas de las lineas</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="BAstrap3/bastrap3/bootstrap.min.css" rel="stylesheet">
	<meta name="viewpoint" content "width=device-width, initial-scale = 1.0">
	<link rel = "stylesheet" type = "text/css" href = "css/boostrapp-responsive.css">
	<link rel="shortcut icon" href="bastrap3/favicon.ico">
    <link rel="apple-touch-icon-precomposed" href="bastrap3/favicon-mobile.png">
    <link rel="stylesheet" href="bastrap3/bootstrap.min.css">
    <link rel="stylesheet" href="bastrap3/bastrap.css">
    <link rel="stylesheet" href="button.css">
	
  </head>
  <body>
  <header class="navbar navbar-primary navbar-top">
  
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-sm-6">
            <a class="navbar-brand bac-header" href="http://www.buenosaires.gob.ar/">Buenos Aires Ciudad</a>
          </div>
		  
          <div class="col-md-6 col-sm-6">
            <h5 class="sub-brand">En todo estás vos</h5>
          </div>	
        </div>
      </div>
    </header>
	
	
    <nav class="navbar navbar-default" role="navigation">
      <div class="container">
        <div class="row">
          <div class="navbar-header">
          </div>
          <div class="collapse navbar-collapse" id="main-nav">
            <ul class="nav navbar-nav navbar-right">
              <h4><a href="index.html">Volver al inicio</a></h4>
            </ul>
          </div>
        </div>
      </div>
    </nav>
		  
 
  
<div align = 'left'>
	<br>
		<h2> Mapas: </h2>
      <div align = "left"> <h3> Linea de Subte: <img src = "Imagenes/Linea_A.png" ></h3></div>
      <div style="width: 100%; overflow-x: auto; overflow-y: hidden;"> <img src = "Imagenes/EsquemaLineaA.jpg" ></div>
      <br>
      <div align = "left"> <h3> Linea de Subte: <img src = "Imagenes/Linea_B_subte.png" ></h3></div>
      <div style="width: 100%; overflow-x: auto; overflow-y: hidden;"> <img src = "Imagenes/EsquemaLineaB.jpg" ></div>
      <br>
      <div align = "left"> <h3> Linea de Subte: <img src = "Imagenes/LineaC.png" ></h3></div>
      <div style="width: 100%; overflow-x: auto; overflow-y: hidden;"> <img src = "Imagenes/EsquemaLineaC.jpg" ></div>
      <br>
      <div align = "left"> <h3> Linea de Subte: <img src = "Imagenes/LineaDLogo.png" ></h3></div>
      <div style="width: 100%; overflow-x: auto; overflow-y: hidden;"> <img src = "Imagenes/EsquemaLineaD.jpg" ></div>
      <br>
      <div align = "left"> <h3> Linea de Subte: <img src = "Imagenes/LineaE.png" ></h3></div>
      <div style="width: 100%; overflow-x: auto; overflow-y: hidden;"> <img src = "Imagenes/EsquemaLineaE.jpg" ></div>
      <br>
      <div align = "left"> <h3> Linea de Subte: <img src = "Imagenes/LineaH.png" ></h3></div>
      <div style="width: 100%; overflow-x: auto; overflow-y: hidden;"> <img src = "Imagenes/EsquemaLineaH.jpg" ></div>
      <br>
      <div align = "left"> <h3> Linea de Subte: <img src = "Imagenes/LineaP.png" ></h3></div>
      <div style="width: 100%; overflow-x: auto; overflow-y: hidden;"> <img src = "Imagenes/EsquemaLineaP.jpg" ></div>
      <br>
	</br>
</div>

<center>
  

</center> 
</div>

<br>
<br>
<br>
<br>
<br>


    <footer>
      <div class="footer">
        <div class="container">
          <div class="row">
            <div class="col-md-6 col-sm-6">
              <a class="navbar-brand bac-footer" href="http://www.buenosaires.gob.ar" target="_blank">Buenos Aires Ciudad</a>
            </div>
            <div class="col-md-6 col-sm-6">
              <div class="sub-brand">
                <p>Dirección General de Gobierno Electrónico<br />
                <span class="text-muted">Ministerio de Modernización E Innovacion Tecnologica</span></p>
				<p>Subtes de la ciudad de Buenos Aires<br/>
				</div>
			  </div>
            </div>
          </div>
        </div>
      </div>
	  
    </footer>
	    <script src="bastrap3/jquery.min.js"></script>
    <script src="bastrap3/bootstrap.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>